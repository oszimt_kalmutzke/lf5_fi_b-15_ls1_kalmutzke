import java.util.ArrayList;
import java.util.Arrays;

public class Spiel {
    private static ArrayList<Raumschiff> Spiel = new ArrayList<>();

    public static void main(String[] args) {
        Raumschiff klingonen = new Raumschiff(
                "Enterprise",
                100.0,
                100.0,
                100.0,
                100.0,
                1,
                2,
                new ArrayList<>(
                        Arrays.asList(
                                new Ladung("klingonen", 200),
                                new Ladung("romulaner", 300)
                        )
                )
        );

        Raumschiff romulaner = new Raumschiff(
                "IRW Khazara",
                100.0,
                100.0,
                100.0,
                100.0,
                2,
                2,
                new ArrayList<>(
                        Arrays.asList(
                                new Ladung("Borg-Schrott", 5),
                                new Ladung("Rote Materie", 2),
                                new Ladung("Plasma-Waffe", 50)
                        )
                )
        );

        Raumschiff vulkanier = new Raumschiff(
                "Ni'Var",
                80.0,
                80.0,
                50.0,
                100.0,
                0,
                5,
                new ArrayList<>(
                        Arrays.asList(
                                new Ladung("Forschungssonde", 35),
                                new Ladung("Photonentorpedo", 3)
                        )
                )
        );

        // Die Klingonen schie�en mit dem Photonentorpedo einmal auf die Romulaner.
        klingonen.shootPhotonTorpedoes(romulaner);
        // Die Romulaner schie�en mit der Phaserkanone zur�ck.
        romulaner.shootPhaserCanons(klingonen);
        // Die Vulkanier senden eine Nachricht an Alle �Gewalt ist nicht logisch�.
        vulkanier.notifyAll("Gewalt ist nicht logisch");
        // Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus. (Geschieht beides in printStatus())
        klingonen.printStatus();
        // Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein.
        vulkanier.sendRepairCommand(true, true, true, vulkanier.getAndroids());
        // Die Vulkanier verladen Ihre Ladung �Photonentorpedos� in die Torpedor�hren Ihres Raumschiffes und r�umen das Ladungsverzeichnis auf.
        vulkanier.loadPhotonTorpedoesFromLadung(3);
        // Die Klingonen schie�en mit zwei weiteren Photonentorpedo auf die Romulaner.
        klingonen.shootPhotonTorpedoes(romulaner);
        klingonen.shootPhotonTorpedoes(romulaner);
        // Die Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
        klingonen.printStatus();
        romulaner.printStatus();
        vulkanier.printStatus();
        // Geben Sie den broadcastKommunikator aus.
        Raumschiff.printFullLog();
    }
}
