import java.util.ArrayList;
import java.util.Objects;

public class Raumschiff {
    private String name = "";
    private double energySupply = 0.0;
    private double shields = 0.0;
    private double hull = 0.0;
    private double lifeSupportSystem = 0.0;
    private int photonTorpedoes = 0;
    private int androids = 0;
    private final ArrayList<Ladung> LadungManifest = new ArrayList<>();
    private static final ArrayList<String> broadcastCommunicator = new ArrayList<>();

    public Raumschiff(){}

    public Raumschiff(
            String name,
            double energySupply,
            double shields,
            double hull,
            double lifeSupportSystem,
            int photonTorpedoes,
            int androids,
            ArrayList<Ladung> Ladungs
    ) {
        this.name = name;
        this.energySupply = energySupply;
        this.shields = shields;
        this.hull = hull;
        this.lifeSupportSystem = lifeSupportSystem;
        this.photonTorpedoes = photonTorpedoes;
        this.androids = androids;
        this.LadungManifest.addAll(Ladungs);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getEnergySupply() {
        return energySupply;
    }

    public void setEnergySupply(double energySupply) {
        this.energySupply = energySupply;
    }

    public double getShields() {
        return shields;
    }

    public void setShields(double shields) {
        this.shields = shields;
    }

    public double getHull() {
        return hull;
    }

    public void setHull(double hull) {
        this.hull = hull;
    }

    public double getLifeSupportSystem() {
        return lifeSupportSystem;
    }

    public void setLifeSupportSystem(double lifeSupportSystem) {
        this.lifeSupportSystem = lifeSupportSystem;
    }

    public int getPhotonTorpedoes() {
        return photonTorpedoes;
    }

    public void setPhotonTorpedoes(int photonTorpedoes) {
        this.photonTorpedoes = photonTorpedoes;
    }

    public int getAndroids() {
        return androids;
    }

    public void setAndroids(int androids) {
        this.androids = androids;
    }

    public ArrayList<Ladung> getLadungManifest() {
        return LadungManifest;
    }

    public void loadLadung(Ladung Ladung) {
        this.LadungManifest.add(Ladung);
    }

    public void shootPhotonTorpedoes(Raumschiff target) {
        if (this.photonTorpedoes == 0) {
            System.out.print("Keine Photonentorpedos gefunden!\n\n");
            this.notifyAll("-=*Click*=-");
            return;
        }

        this.photonTorpedoes -= 1;
        this.notifyAll("Photonentorpedo abgeschossen");
        target.registerDamage();
    }

    public void shootPhaserCanons(Raumschiff target) {
        if (this.energySupply < 50.0) {
            this.notifyAll("-=*Click*=-");
            return;
        }

        this.energySupply -= 50.0;
        this.notifyAll("Phaserkanone abgeschossen");
        target.registerDamage();
    }

    private void registerDamage() {
        System.out.print(this.name + " wurde getroffen!\n\n");
        if (this.shields > 0.0) {
            this.shields -= 50.0;
        }

        if (this.shields <= 0.0) {
            this.shields = 0.0;
            this.hull -= 50.0;
            this.energySupply -= 50.0;

            if (this.energySupply < 0.0) {
                this.energySupply = 0.0;
            }
        }

        if (this.hull <= 0.0) {
            this.hull = 0.0;
            this.lifeSupportSystem = 0.0;
            this.notifyAll("Lebenserhaltungssysteme wurden vernichtet.");
        }
    }

    public void notifyAll(String message) {
        broadcastCommunicator.add("\n" + this.name + ": " + message);
    }

    public void printStatus() {
        System.out.printf(
                "%s:\nEnergy supply: %.2f\nShields: %.2f\nHull: %.2f\nLife support system: %.2f\nPhoton torpedoes: %d\nAndroids: %d\n",
                this.name,
                this.energySupply,
                this.shields,
                this.hull,
                this.lifeSupportSystem,
                this.photonTorpedoes,
                this.androids
        );
        System.out.print("Ladung manifest:\n");
        this.LadungManifest.forEach(Ladung -> {
            System.out.print(Ladung.toString());
        });
    }

    public static void printFullLog() {
        System.out.printf("Logs:%s", broadcastCommunicator.toString().replaceAll("[\\[\\],]", "" ));
    }

    public void loadPhotonTorpedoesFromLadung(int amount) {
        for (Ladung Ladung : this.LadungManifest) {
            if (Objects.equals(Ladung.getName(), "Photonentorpedo")) {
                if (Ladung.getAmount() < amount) {
                    amount = Ladung.getAmount();
                }

                Ladung.setAmount(Ladung.getAmount() - amount);
                this.photonTorpedoes += amount;
                System.out.print(amount + " Photonentorpedo(s) eingesetzt\n\n");
            }
        }

        this.cleanUpLadungManifest();
    }

    public void sendRepairCommand(boolean repairShield, boolean repairEnergySupply, boolean repairHull, int amountAndroids) {
        int randomInteger = (int) (Math.random() * 100);
        int structuresToBeRepaired = 0;
        if (repairShield) {
            structuresToBeRepaired += 1;
        }

        if (repairEnergySupply) {
            structuresToBeRepaired += 1;
        }

        if (repairHull) {
            structuresToBeRepaired += 1;
        }

        if (repairShield) {
            this.shields += (double) (randomInteger * Math.min(amountAndroids, this.androids)) / structuresToBeRepaired;
        }

        if (repairEnergySupply) {
            this.energySupply += (double) (randomInteger * Math.min(amountAndroids, this.androids)) / structuresToBeRepaired;
        }

        if (repairHull) {
            this.hull += (double) (randomInteger * Math.min(amountAndroids, this.androids)) / structuresToBeRepaired;
        }
    }

    public void printLadung() {
        this.LadungManifest.forEach(Ladung -> {
            System.out.print(Ladung.toString());
        });
    }

    public void cleanUpLadungManifest() {
        this.LadungManifest.removeIf(Ladung -> Ladung.getAmount() <= 0);
    }
}
