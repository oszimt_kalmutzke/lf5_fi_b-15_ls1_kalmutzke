public class Kapitaen {

    /** Name des Kapitaen. */
    private String name = "";
    /** Seitwann ist der Kapitaen der Kapitaen. */
    private int KapitaenSince = 0;
    /** Von welchem Raumschiff ist der Kapitean der Kapitean. */
    private String KapitaenOf = "";

    public Kapitaen() {}

    public Kapitaen(String name, int KapitaenSince, String KapitaenOf) {
        this.name = name;
        this.KapitaenSince = KapitaenSince;
        this.KapitaenOf = KapitaenOf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getKapitaenSince() {
        return KapitaenSince;
    }

    public void setKapitaenSince(int KapitaenSince) {
        this.KapitaenSince = KapitaenSince;
    }

    public String getKapitaenOf() {
        return KapitaenOf;
    }

    public void setKapitaenOf(String KapitaenOf) {
        this.KapitaenOf = KapitaenOf;
    }
}
