public class Ladung {
    private String name = "";
    private int amount = 0;

    public Ladung(){}

    public Ladung(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + ", Name: " + this.name + ", Amount: " + this.amount + "\n\n";
    }
}
